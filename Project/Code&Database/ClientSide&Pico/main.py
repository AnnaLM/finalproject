
wifiname= "BT-ABCD"
wifipassword="MyWifiPassword"

#"BT-MRA376","RKuJdCY9mT9xip"

##################################################################################################
##################################################################################################
##################################################################################################
LocationID=1 #different for each location, same as database
##################################################################################################

import utime 
from machine import Pin, I2C #type of input used
import adafruit_bme680 # imports the library for the bme680
from struct import *
import uos #operating system

i2c = I2C(0) # sets the type on input being used
bmeSensor = adafruit_bme680.BME680_I2C(i2c) # sets the bme680 to use i2c input too

server_ip="192.168.1.251" # the server computer
server_port=9999 # the port used

uart0 = machine.UART(0, baudrate=115200) # either 115200 or 9600 - depending on hardware specs

def sendCMD_waitResp(cmd, uart=uart0, timeout=2000): # sends a command to server
    uart.write(cmd)
    waitResp(uart, timeout)
    
def waitResp(uart=uart0, timeout=2000): # confirms the server recieved the command
    prvMills = utime.ticks_ms()
    resp = b""
    while (utime.ticks_ms()-prvMills)<timeout:
        if uart.any():
            resp = b"".join([resp, uart.read(1)])
    print("resp:")
    try:
        print(resp.decode()) # the signal recieved is changed into an interpretable responde 
    except UnicodeError:
        print(resp)

def sendCMD_waitAndShow(cmd, uart=uart0): #allows for communication to and from server
    print("CMD: " + cmd)
    uart.write(cmd)
    while True:
        print(uart.readline()) 
        
def espSend(text="test", uart=uart0):#sends selected text to server 
    sendCMD_waitResp('AT+CIPSEND=' + str(len(text)) + '\r\n')
    sendCMD_waitResp(text)
    
sendCMD_waitResp('AT\r\n')          #Test AT startup
sendCMD_waitResp('AT+GMR\r\n')      #Check version information
sendCMD_waitResp('AT+CWMODE?\r\n')  #Query the Wi-Fi mode
sendCMD_waitResp('AT+CWMODE=1\r\n') #Set the Wi-Fi mode 1 = Station mode
sendCMD_waitResp('AT+CWMODE?\r\n')  #Query the Wi-Fi mode again
sendCMD_waitResp('AT+CWJAP="BT-MRA376","RKuJdCY9mT9xip",\r\n', timeout=5000)#Connect to AP
sendCMD_waitResp('AT+CIFSR\r\n')    #Obtain the Local IP Address
sendCMD_waitResp('AT+CIPSTART="TCP","' +server_ip +'",' + str(server_port) +'\r\n')
espSend()

while True:
    sendCMD_waitResp('AT+CIPSTART="TCP","' +server_ip +'",' +str(server_port) +'\r\n') # connects to the server
    now = utime.time() # gets time
    tm = utime.localtime(now)
    if (tm[4] <1): # checks that the time entity is an hour
        temp = bmeSensor.temperature #read temperature
        humidity = bmeSensor.humidity #read humidity
        pressure = bmeSensor.pressure #read pressure
        gas= bmeSensor.gas #read gas
        f = open("saved.txt", "a") #save it locally
        data = ("T "+str(temp)+", H "+str(humidity)+", P "+str(pressure)+", G "+str(gas)+"\n") #the data to be saved 
        f.write(data)#saving data
        f.close()
        espSend(data) #send the information to the server
    utime.sleep(60)
