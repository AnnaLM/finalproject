import socketserver
import platform
import sqlite3#connects to sqlite


class MyTCPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        conn = sqlite3.connect('AirQuality.db') #name of database
        self.data = self.request.recv(1024).strip() 
        print("{} wrote:".format(self.client_address[0]))# if uncommented shows recieved data
        print(self.data)
        temp  = (self.data)
        temp=temp.decode() #changes into readable signal
        temp = temp.replace('T', '')# string handles
        temp = temp.replace('H', '')
        temp = temp.replace('P', '')
        temp = temp.replace('G', '')
        temp = temp.replace('ID', '')
        temp = temp.replace(' ', '')
        temp=temp.split(',')
        print(temp)
        conn.execute("INSERT INTO HOME (Temperature,Pressure,Gas,Humidity,LocationID) \
              VALUES ("+temp[0]+","+temp[2]+","+temp[3]+","+temp[1]+","+temp[4]+")");
        conn.commit()#adds to the database 
        conn.close()#saves to database

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999 #sets settings for the clients to connect to 
    with socketserver.TCPServer(('', PORT), MyTCPHandler) as server:
        server.serve_forever() #repeat indefinetly
