import sqlite3 #connects to database
import time
import datetime
import random

#this code can be altered to change database or entity,  this file acts as a skeleton example of how data got extracted

conn = sqlite3.connect('AirQuality.db')
c = conn.cursor()

Depression = [] # creates a list of weekly scored which can then be iterated (summed or averaged)for anaylsis
DepressionInts=[]

Anxiety = []
AnxietyInts=[]
def DepressionScores():
    #sums all the scores in for each week
    # an extra –––WHERE Participants.LocationID() = "1"––– to specificy total from a location
    c.execute("select SUM(DepressionScores.Week1) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week2) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week3) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week4) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week5) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week6) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week7) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week8) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week9) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    c.execute("select SUM(DepressionScores.Week10) from DepressionScores inner join Participants on Participants.NHSDepressionID = DepressionScores.ID;")
    Depression.append (c.fetchall())
    for score in Depression: #string handeling to be graphable(via plotly)/readable
        current=str(score[0])
        current=current.strip("(,)")
        DepressionInts.append(int(current))
    print (DepressionInts)


def AnxietyScores():
    c.execute("select SUM(AnxietyScores.Week1) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week2) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week3) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week4) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week5) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week6) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week7) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week8) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week9) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    c.execute("select SUM(AnxietyScores.Week10) from AnxietyScores inner join Participants on Participants.NHSAnxietyID = AnxietyScores.ID;")
    Anxiety.append (c.fetchall())
    for score in Anxiety: 
        current=str(score[0])
        current=current.strip("(,)")
        AnxietyInts.append(int(current))
    print (AnxietyInts)


DepressionScores()
AnxietyScores()
c.close
conn.close()


